// pages/backGoods/index.js
Page({
    data: {
        color: wx.getExtConfigSync ? wx.getExtConfigSync().color : {},
        showReason: false,
        backReason: [],
        Q_description: '',  // 问题描述
        reasonIndex: -1   // 退货原因选项
    },
    onLoad(option) {
        if(option.data) {
            wx.setNavigationBarTitle({
                title: option.type==1 ? '退款申请' : '申请售后'
            });
            this.setData({
                type: option.type,
                backData: JSON.parse(option.data)
            })
        }
    },
    onShow() {
        this.getBackReason();
    },
    getBackReason() {
        wx.doodoo.fetch(`/shop/api/shop/order/service/index?type=${this.data.type}`).then(res => {
            if(res && res.data.errmsg == 'ok') {
                this.setData({
                    backReason: res.data.data
                })
            }
        })
    },
    bindinput(e) {
        this.setData({
            Q_description: e.detail.value,
            Q_length: e.detail.cursor
        })
    },
    hideModal() {
        if (!this.data.service_id) {
            this.setData({
                reason: ''
            })
        }
        let animation = wx.createAnimation({
            duration: 200,
            timingFunction: 'linear'
        })
        animation.translateY(0).step();
        this.setData({
            animationData: animation.export()
        })
        setTimeout(() => {
            animation.translateY(-335).step();
            this.setData({
                animationData: animation.export(),
                showSku: false
            })
        }, 200)
    },
    showModal(e) {
        // 0：加入购物车  1：立即购买
        let animation = wx.createAnimation({
            duration: 200, // 动画持续时间
            timingFunction: 'linear' // 定义动画效果，当前是匀速
        })
        animation.translateY(0).step();
        this.setData({
            animationData: animation.export(), // 通过export()方法导出数据
            showSku: true
        })
        // 设置setTimeout来改变y轴偏移量，实现有感觉的滑动
        setTimeout(() => {
            animation.translateY(-335).step();
            this.setData({
                animationData: animation.export()
            })
        }, 200)
    },
    submit() {
        if(this.data.reasonIndex<0) {
            wx.showModal({
                title: '提示',
                content: '请选择退货原因',
                showCancel: false
            })
            return;
        }
        wx.doodoo.fetch(`/shop/api/shop/order/service/apply`,{
            method: 'POST',
            data: {
                id: this.data.backData.id,
                service_id: this.data.service_id,
                type: this.data.type,
                content: this.data.Q_description
            }
        }).then(res => {
            if(res && res.data.errmsg == 'ok') {
                wx.navigateBack();
            }
        })
    },
    goDetail() {
        wx.navigateTo({
            url: `/pages/shop/product/product-detail/index?id=${this.data.backData.id}`
        });
    },
    selectReason(e) {
        const data = e.currentTarget.dataset.data;
        const reasonIndex = e.currentTarget.dataset.index;
        this.setData({
            selectItem: data,
            reason: data.name,
            reasonIndex: reasonIndex
        })
    },
    chooseReason(e) {
        this.setData({
            reason: this.data.selectItem.name,
            service_id: this.data.selectItem.id
        })
        this.hideModal();
    }
})