const product = require("./product");

module.exports = doodoo.bookshelf.Model.extend({
    tableName: "shop_user_collection",
    hasTimestamps: true,
    product: function() {
        return this.morphTo("collection", product);
    }
});
