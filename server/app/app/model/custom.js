const custom_group = require("./custom_group");

module.exports = doodoo.bookshelf.Model.extend({
    tableName: "custom",
    hasTimestamps: true,
    hidden: ["password_org"],
    group: function() {
        return this.belongsTo(custom_group, "group_id");
    }
});
